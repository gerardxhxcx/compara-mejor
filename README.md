# Prueba frontend comparamejor

Prueba de desarrollo front end de un menú consumiendo un JSON desde una URL localhost:9000/menu

## Tecnologías

* ES6
* Webpack
* Stylus
* Jquery

## Ejecución:

* El proyecto debe correr ejecutando solo el comando `npm i && npm run start`
