var path = require('path');
var webpack = require('webpack');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var CopyWebpackPlugin = require('copy-webpack-plugin');
var OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');

var stylusLoader = ExtractTextPlugin.extract({
	fallback: "style-loader",
	use: ['css-loader','stylus-loader?resolve url']
});


var config = {
	entry: {
		main: "./assets/lib/main.js",
		addscripts: "./assets/lib/addscripts.js"
	},
	output: {
		path: path.join(__dirname, "public"),
		filename: "js/[name].js"
	},
	devtool: 'source-map',
	module: {
			loaders: [{
				test: [/\.js$/,/\.es6$/,/\.jsx?$/],
				exclude: /(node_modules|bower_components)/,
				use: {
					loader: 'babel-loader',
					query: {
						presets: ['env'],
						plugins: ['transform-es2015-modules-commonjs','transform-regenerator', 'transform-runtime']
					}
				}
			},
			{
				test: /\.styl$/,
				loader: stylusLoader
			},
			{
				test: /\.(png|jpg|svg|ttf|eot|woff|woff2)$/,
				loader: 'file?name=[path][name].[ext]'
			},
			{
				test: /\.json$/,
				loader: 'json-loader',
			}]
	},
	plugins: [
		new ExtractTextPlugin("css/[name].min.css"),
		new webpack.optimize.CommonsChunkPlugin({
			name: 'main',
			filename: 'js/app.min.js'
		}),
		new CopyWebpackPlugin([
			{from: './assets/fonts', to: 'fonts'},
			{from: './assets/img', to: 'img'},
			// {from: './assets/json', to: 'json'},
			// {from: './assets/media', to: 'media'}
		]),
		new webpack.optimize.UglifyJsPlugin({
			mangle: {
				except: ['$']
			},
			compress: { warnings: false },
			output: {comments: false},
			include: /\.min\.js$/,
			sourceMap: true
		}),
		new OptimizeCssAssetsPlugin({
			assetNameRegExp: /\.min\.css$/,
			cssProcessorOptions: { discardComments: { removeAll: true } }
		}),
	]
};

if (process.env.NODE_ENV === 'production') {
	config.plugins.push(
	  new webpack.optimize.DedupePlugin(),
	  new webpack.optimize.UglifyJsPlugin({
		compress: {
		  warnings: false,
		},
		mangle: {
		  except: ['$super', '$', 'exports', 'require'],
		},
	  })
	);
};

module.exports = config;
