
/*javascript*/
require('./main.styl');

require('es6-promise').polyfill();
require('isomorphic-fetch');

// var $ = require('../../node_modules/jquery');
var $ = require('jquery');
window.jQuery = $;
window.$ = $;

fetch('/menu')
	.then(function(response) {
		if (response.status >= 400) {
			throw new Error("Bad response from server");
		}
		return response.json();
	})
	.then(function(menu) {

	  let li = (obj, ind = -1) => `
	  	<li class="
			${ind != -1 ? `parent-link `:``}
			${obj.submenu.length ? `dropdown`:``}
		">
         <a href="${obj.url}">
            <span>
                  ${ind != -1 ? `<i class="icon icon-${obj.name}"></i>`:``}
                  ${obj.name}
            </span>
         </a>
         ${obj.submenu.length ? `<i class="icon icon-arrow-down"></i>` : ``}
         ${obj.submenu.length ? `<ul>` : ``}
         ${obj.submenu.map((obj) => li(obj)).join('\n')}
         ${obj.submenu.length ? `</ul>` : ``}
      </li>`;

      let html = (menu) => `
		<header class="main-nav">
			<div class="container">
				<div class="menu-nav">
					<a href="/"><img class="logo" src="/img/comparamejor.svg" alt="compara mejor"></a>
					<div class="ctrl-nav">
						<span></span>
						<span></span>
						<span></span>
					</div>
					<nav>
						<ul>
							${menu.map((obj, ind) => li(obj, ind)).join('\n')}
						</ul>
					</nav>
		      </div>
			</div>
		</header>`;

      document.getElementsByClassName('wrapper')[0].innerHTML = html(menu).replace(/[\n\r]\s\s+/gm, '');
	});

(($) => {
	$(document).on('click', '.ctrl-nav', function(e){
		e.preventDefault();
		$(this).stop().toggleClass('active');
		$('.main-nav nav').stop().slideToggle(500, () => {
			$('.main-nav nav').toggleClass('mobile-active').removeAttr('style');
		});
	});

	$(document).on('click', '.dropdown .icon', function(e){
		e.preventDefault();
		$(this).parents('.dropdown').stop().toggleClass('active');
		$(this).parents('.dropdown').siblings('.dropdown').removeClass('active');
		$(this).parents('.dropdown').siblings('.dropdown').find('ul').hide();
		$(this).next('ul').stop().slideDown(300);
	});

	$(window).resize(() => {
		let windowWidth = window.innerWidth;
		if(windowWidth > 720){
			$('.main-nav .ctrl-nav').removeClass('active');
			$('.main-nav nav').removeClass('mobile-active');
			$('.dropdown').removeClass('active');
			$('.dropdown ul').removeAttr('style');
		}
	});
})(jQuery)
